package Abstraction;

import java.util.Scanner;

public class mainApp1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Account Type");
        System.out.println("1.Saving/n2.loan");
        int Acctype=sc1.nextInt();
        System.out.println("Enter The Account Opening Amount");
        double balance=sc1.nextDouble();
        AccountFactory factory=new AccountFactory();
       Account AccRef= factory.createAccount(Acctype,balance);


        boolean status =true;
        while(status){
            System.out.println("Select The Transaction Mode");
            System.out.println("1.Deposite/n2.Withdraw/n3.Checkbal/n4.Exit");
            int choice = sc1.nextInt();


            if(choice==1)
            {
                System.out.println("Enter Amount");
                double amt=sc1.nextDouble();
                AccRef.deposite(amt);
            } else if (choice==2) {
                System.out.println("Enter Amount");
                double amt=sc1.nextDouble();
                AccRef.withdraw(amt);

            } else if (choice==3) {
                AccRef.checkbal();

            }
            else {
                status=false;
            }
        }
    }
}

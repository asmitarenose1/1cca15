package Abstraction;

public class LoanAccount implements Account{
    double loanAmount;
    public LoanAccount( double loanAmount)
    {
        this.loanAmount=loanAmount;
    }
    @Override
    public void deposite(double amt) {
        loanAmount-=amt;
        System.out.println(amt+" Rs is Debited From Loan Account");

    }

    @Override
    public void withdraw(double amt) {
        loanAmount+=amt;
        System.out.println(amt+" Rs is Credited From Your Account");

    }

    @Override
    public void checkbal() {
        System.out.println("Active Balance is :"+loanAmount);

    }
}

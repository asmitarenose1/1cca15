package Abstraction;

public class savingAccount implements Account{

   double accountBalance;
   public savingAccount(double accountBalance)
   {
       this.accountBalance=accountBalance;
       System.out.println("Saving Account Created");
   }
    @Override
    public void deposite(double amt) {
        accountBalance+=amt;
        System.out.println(amt+" Rs is credited to your account");

    }

    @Override
    public void withdraw(double amt) {
        if(accountBalance>=amt)
        {
            accountBalance-=amt;
            System.out.println(amt+" Rs is Debited From Your Account ");
        }
        else
        {
            System.out.println("INSUFFICIENT AMOUNT");
        }

    }

    @Override
    public void checkbal() {
        System.out.println("Active Balance is :"+accountBalance);

    }
}
